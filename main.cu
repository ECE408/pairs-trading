#define BLOCK_SIZE 1024 //For 1D kernel functions
#define BLOCK_SIZE_SMALL 32 //For 2D kernel functions
#define TILE_WIDTH 32 //For tiled matrix multiplication

#define wbCheck(stmt)                                                          \
  do {                                                                         \
    cudaError_t err = stmt;                                                    \
    if (err != cudaSuccess) {                                                  \
      wbLog(ERROR, "Failed to run stmt ", #stmt);                              \
      wbLog(ERROR, "Got CUDA error ...  ", cudaGetErrorString(err));           \
      return -1;                                                               \
    }                                                                          \
  } while (0)

#include "vecOp.cu"
#include "signalmatrix.cu"
#include "signalmatrix1k.cu"
#include "signalfix.cu"
#include "matrixmultiply.cu"
#include "wb.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <algorithm>   
using namespace std;

//For Reading data in a csv file
vector<float> readFloatData( char* fileName )
{
    ifstream ifile(fileName, std::ios::in);
    vector<float> data;

    //check to see that the file was opened correctly:
    if (!ifile.is_open()) {
        //std::cerr << "There was a problem opening the input file!\n";
        exit(1);//exit or do additional error checking
    }

    float num = 0.0;
    //keep storing values from the text file so long as data exists:
    while (ifile >> num) {
        data.push_back(num);
    }

    return data;
}

//For writing data into a csv file
void writeData( float* Array)
{
    ofstream Data;
    Data.open("Profit.csv");
	
    cout << sizeof(Array)<< endl;
	
    for (int i = 0; i < sizeof(Array)/sizeof(float); i++)
        Data << Array[i] << endl;
}
	

/* Finds the mean of the signal Matrix
   Input: deviceRatios, Output memory, output size, adn vector size
   Output: Mean*/ 
float findMean(float* deviceRatios, float* hostOutput, float* deviceOutput,int numOutputElements, int vectorSize)
{ 	
    dim3 dimGrid(numOutputElements);
    dim3 dimBlock(BLOCK_SIZE);
    
    // Find the total sum of the array using parallel reduce
    total<<<dimGrid, dimBlock>>>(deviceRatios, deviceOutput, vectorSize);
    cudaDeviceSynchronize();

    cudaMemcpy(hostOutput, deviceOutput, numOutputElements * sizeof(float), cudaMemcpyDeviceToHost);
	
    //Reduce output vector on the host to find the absolute sum
    for (int ii = 1; ii < numOutputElements; ii++) {
        hostOutput[0] += hostOutput[ii];
    }
	
    //mean is the total/size of array
    float mean = hostOutput[0] / (vectorSize);
    cout << "mean: " << mean << endl;
    	
    return mean;
}

int findHeight(	float* deviceRatios, float* hostOutput, float* deviceOutput, int numOutputElements, int vectorSize, float mean)
{ 
    dim3 dimGrid(numOutputElements);
    dim3 dimBlock(BLOCK_SIZE);
    
    //Find the max of the Price ratio using parallel reduce
    max<<<dimGrid, dimBlock>>>(deviceRatios, deviceOutput, vectorSize);
    cudaDeviceSynchronize();

    cudaMemcpy(hostOutput, deviceOutput, numOutputElements * sizeof(float), cudaMemcpyDeviceToHost);
    
	
    //Reduce output vector on the host to find the abs max
    for (int ii = 1; ii < numOutputElements; ii++) {
        hostOutput[0] = max(hostOutput[0], hostOutput[ii]);
    }
    
    float vecmax = hostOutput[0];
 	
    //Find the min of the Price ratio using parallel reduce
    min<<<dimGrid, dimBlock>>>(deviceRatios, deviceOutput, vectorSize);
    cudaDeviceSynchronize();

    cudaMemcpy(hostOutput, deviceOutput, numOutputElements * sizeof(float), cudaMemcpyDeviceToHost);
	
    //Reduce output vector on the host to find the abs min
    for (int ii = 1; ii < numOutputElements; ii++) {
        hostOutput[0] = min(hostOutput[0], hostOutput[ii]);
    }
 	
    float min = hostOutput[0];
 	
    /*The height of the size matrix is the number of k values we will try and is the largest difference value from the mean
    Our k increment is .01 so the largest difference will be divided by the increment to find out the actual height*/
    int sigMatrixHeight = (int) (max(vecmax - mean, mean - min) /.01);
    
    // Print max, min, # of ks
    cout << "max: " << vecmax << endl;
    cout << "min: " << min << endl;
    
    return sigMatrixHeight;
}

/*Find and return the value of the k that gives the most profit 
Inputs: Stock Prices, Signal Matrix, properties of the signal matrix
Output: BestK Index*/
int findBestK(float* deviceStockA, float* deviceStockB, float* deviceSignalMatrix, float mean, int sigMatrixHeight, int vectorSize)
{
    float *hostProfitA;
    float *hostProfitB;    
    float *hostProfitTotal;
    float *deviceProfitA;
    float *deviceProfitB;
    float *deviceProfitTotal;
    
    // Allocate an array in the host for the profit of size sigMatrixHeight. We are finding the total profit for each row(k) of the signal matrix
    hostProfitA = (float*) malloc(sizeof(float)*sigMatrixHeight);
    hostProfitB = (float*) malloc(sizeof(float)*sigMatrixHeight);
    hostProfitTotal = (float*) malloc(sizeof(float)*sigMatrixHeight);
		
    // Allocate an array in the device for the profit of size sigMatrixHeight. We are finding the total profit for each row(k) of the signal matrix
    cudaMalloc((void**)&deviceProfitA, sizeof(float)*sigMatrixHeight);
    cudaMalloc((void**)&deviceProfitB, sizeof(float)*sigMatrixHeight);
    cudaMalloc((void**)&deviceProfitTotal, sizeof(float)*sigMatrixHeight);
	
    dim3 dimGrid(1,(sigMatrixHeight-1) / TILE_WIDTH + 1);
    dim3 dimBlock(TILE_WIDTH,TILE_WIDTH);
    
    //Do tiled matrix multiplication to find the Profit of Stock A for all k values
    matrixMultiplyShared<<<dimGrid,dimBlock>>>(deviceSignalMatrix, deviceStockA, deviceProfitA, sigMatrixHeight, 
												 vectorSize, vectorSize, 1, sigMatrixHeight, 1);
				
    //Do tiled matrix multiplication to find the Profit of Stock B for all k values								 
    matrixMultiplyShared<<<dimGrid,dimBlock>>>(deviceSignalMatrix, deviceStockB, deviceProfitB, sigMatrixHeight, 
												 vectorSize, vectorSize, 1, sigMatrixHeight, 1);												 
	
    //Subtract the Profit of A from B to find Total Profit
    vecSub<<<(int)ceil((float)sigMatrixHeight / BLOCK_SIZE), BLOCK_SIZE>>>(deviceProfitB, deviceProfitA, deviceProfitTotal, sigMatrixHeight);
   
    cudaMemcpy(hostProfitA, deviceProfitA, sizeof(float)*sigMatrixHeight, cudaMemcpyDeviceToHost);
    cudaMemcpy(hostProfitB, deviceProfitB, sizeof(float)*sigMatrixHeight, cudaMemcpyDeviceToHost);
	
    //Copy the Profits over to host
    cudaMemcpy(hostProfitTotal, deviceProfitTotal, sizeof(float)*sigMatrixHeight, cudaMemcpyDeviceToHost);
	
    //Declare variables for MaxProfit and BestK. The BestK corresponds to the index in hostProfitTotal that gives the most profit
    float bestProfit = *(max_element(hostProfitTotal, hostProfitTotal + sigMatrixHeight));  
    int bestK = distance(hostProfitTotal, max_element(hostProfitTotal, hostProfitTotal + sigMatrixHeight));

    //Print Index and value of BestK
    cout << "Index of Best k: " << bestK << endl;
    cout << "value of Best k: " <<  .01*(bestK+1)<< endl;
  	
    //Print mean ± k (This is where we want to trade for future trading)
    cout << "Price Ratio corresponding to the best k: " << mean << " ± " << .01*(bestK+1) << endl; 	
  	
    //Print the best profit corresponding to the best k value	
    cout << endl << "Profit made from Best k: " << bestProfit << endl;
   
    
    // Release device memory and host memory used for the function
    free(hostProfitA);
    free(hostProfitB);
    free(hostProfitTotal);
    cudaFree(deviceProfitA);
    cudaFree(deviceProfitB);
    cudaFree(deviceProfitTotal);
    cudaFree(deviceSignalMatrix);
    
    //Return the best K Value
    return bestK;
}

/*Find and return the profit corresponding to the Signal Array of bestK
Inputs: Stock Prices, Signal Array, mean of signal array, vector size, bestK
Output: Profit */
void profitOfBestK(float* deviceStockA, float* deviceStockB, float* deviceSignalMatrix, float mean,  int vectorSize, int k)
{
    float *deviceProfitA;
    float *deviceProfitB;
    float *deviceProfitTotal;
		
    // Allocate an array for the profit of size sigMatrixHeight. We are finding the total profit for each row(k) of the signal matrix
    cudaMalloc((void**)&deviceProfitA, sizeof(float)*vectorSize );
    cudaMalloc((void**)&deviceProfitB, sizeof(float)*vectorSize );
    cudaMalloc((void**)&deviceProfitTotal, sizeof(float)*vectorSize) ;
	
    dim3 dimGrid(BLOCK_SIZE);
    dim3 dimBlock((int)ceil((float)vectorSize / BLOCK_SIZE));
    
    //Do tiled matrix multiplication to find the Profit of Stock A for all k values
    vecMult<<<dimGrid,dimBlock>>>(deviceSignalMatrix, deviceStockA, deviceProfitA, vectorSize);
				
    //Do tiled matrix multiplication to find the Profit of Stock B for all k values								 
    vecMult<<<dimGrid,dimBlock>>>(deviceSignalMatrix, deviceStockB, deviceProfitB, vectorSize);											 
	
    //Subtract the Profit of A from B to find Total Profit
    vecSub<<<dimGrid,dimBlock>>>(deviceProfitB, deviceProfitA, deviceProfitTotal, vectorSize);

    float *hostOutput; // The output list
    float *deviceOutput;
    
    int numOutputElements; // number of elements in the output array for the reductions
  
    //calculate the size of the output for the upcoming parallel reduce functions
    numOutputElements = vectorSize / (BLOCK_SIZE << 1);
    if ((vectorSize) % (BLOCK_SIZE << 1)) {
       numOutputElements++;
    }
    
    hostOutput = (float *)malloc(numOutputElements * sizeof(float));
    cudaMalloc(&deviceOutput, numOutputElements * sizeof(float));
    
    
    // Find the total sum of the array using parallel reduce
    total<<<numOutputElements, BLOCK_SIZE>>>(deviceProfitTotal, deviceOutput, vectorSize);
    cudaDeviceSynchronize();
    cudaMemcpy(hostOutput, deviceOutput, numOutputElements * sizeof(float), cudaMemcpyDeviceToHost);
	
	
    //Reduce output vector on the host to find the absolute sum
    for (int ii = 1; ii < numOutputElements; ii++) {
        hostOutput[0] += hostOutput[ii];
    }
  	
    //Print the best profit corresponding to the best k value	
    cout << endl << "Profit made from k=" << k <<": " << hostOutput[0] << endl;
   
    
    // Release device memory and host memory used for the function
    cudaFree(deviceProfitA);
    cudaFree(deviceProfitB);
    cudaFree(deviceProfitTotal);
    
}

/*Generate a signal matrix Graph using 2D arrays. Mean, max, min will be calculated using Parallel Reduction algorithms. The height of k(# of k's to  45.1082

  test) of the signal matrix will be dependent on those values. The signals will be found using a complicated scan function that will generate 1 and 
  -1 for points that cross k values and a 9 for points that cross the mean. These numbers tell us when we want to trade while every other element will
   be filled with zeroes which are days that we don't trade. This function will return the k value that made the highest profit during this training 
   period which will be used to calculate profit for the testing period.*/
int Generate(float* deviceStockA, float* deviceStockB, float* deviceRatios, int vectorSize)
{
    float *hostOutput; // The output list
    float *deviceOutput;
    
    int numOutputElements; // number of elements in the output array for the reductions
  
    //calculate the size of the output for the upcoming parallel reduce functions
    numOutputElements = vectorSize / (BLOCK_SIZE << 1);
    if ((vectorSize) % (BLOCK_SIZE << 1)) {
       numOutputElements++;
    }
    
    hostOutput = (float *)malloc(numOutputElements * sizeof(float));
    cudaMalloc(&deviceOutput, numOutputElements * sizeof(float));
	
    //Find and Print the mean using reduction
    float mean = findMean(deviceRatios, hostOutput, deviceOutput, numOutputElements, vectorSize);
	
    //Find and print the min, max using reduction. Then use mean, max, and mmin to find the height.
    int sigMatrixHeight = findHeight(deviceRatios, hostOutput, deviceOutput, numOutputElements, vectorSize, mean);
	
    //Ptrint the height
    cout << endl<< "#of k's: " << sigMatrixHeight << endl;

    /********************************Finding and fixing the signals for the matrix********************************************************/	
    float* hostSignalMatrix;
    float* deviceSignalMatrix;
    float* deviceSignalMatrixCopy;

    int sigMatrixNumElements = sigMatrixHeight * vectorSize;
    size_t sigMatrixNumBytes = sigMatrixNumElements * sizeof(float);
    
    hostSignalMatrix = (float *)malloc(sigMatrixNumBytes);
    cudaMalloc(&deviceSignalMatrix, sigMatrixNumBytes);
    cudaMalloc(&deviceSignalMatrixCopy, sigMatrixNumBytes);
    
    dim3 dimGrid((int)ceil(((float)vectorSize /BLOCK_SIZE_SMALL)),(int)ceil(((float)sigMatrixHeight / BLOCK_SIZE_SMALL)));
    dim3 dimBlock(BLOCK_SIZE_SMALL,BLOCK_SIZE_SMALL);

    cudaMemcpy( deviceSignalMatrix, hostSignalMatrix, sigMatrixNumBytes, cudaMemcpyHostToDevice);
    
    //Create the signal matrix. Puts -1 everytime a point crosses mean+k ,1 everytime a point crosses mean-k, 9 everytime it crosses the mean 
    signalMatrix<<<dimGrid, dimBlock>>>(deviceRatios, deviceSignalMatrix, mean, vectorSize, sigMatrixHeight);
    cudaDeviceSynchronize();

    cudaMemcpy(deviceSignalMatrixCopy, deviceSignalMatrix, sigMatrixNumBytes, cudaMemcpyDeviceToDevice);

    //Apply our trading strategy by fixing the signal matrix by changing the 999s to the value of adding the previous 1s and -1s until the previous 999
    wbTime_start(Generic, "Fixing Signal"); 
    signalFixGlobal<<<dimGrid, dimBlock>>>(deviceSignalMatrix, deviceSignalMatrixCopy, vectorSize, sigMatrixHeight);
	//signalFix<<<dimGrid, dimBlock>>>(deviceSignalMatrix, deviceSignalMatrixCopy, vectorSize, sigMatrixHeight);
    wbTime_stop(Generic, "Fixing Signal"); 
    cudaDeviceSynchronize();
    	
    cudaMemcpy(hostSignalMatrix, deviceSignalMatrix, sigMatrixNumBytes, cudaMemcpyDeviceToHost);
    
    // Release device memory and host memory used for the function
    free(hostOutput);
    cudaFree(deviceOutput);
    
    //Use the function to find the bestK value given the fixed signal matrix and return it
    return findBestK(deviceStockA, deviceStockB, deviceSignalMatrix, mean, sigMatrixHeight, vectorSize);
}

/*Generate a signal array for 1 k value. Only mean needs to be calculated because height of the signal matrix will be 1. 
Creates a signal array using the signal matrix and signal fix functions for a 1D array.using 2D arrays. Then it will call the profitOfBestK function
which will return the profit given the signal array corresponding to the best k*/
void Generate(float* deviceStockA, float* deviceStockB, float* deviceRatios, int vectorSize, int bestK)
{
    float *hostOutput; // The output list
    float *deviceOutput;
    
    int numOutputElements; // number of elements in the output array for the reductions
  
    //calculate the size of the output for the upcoming parallel reduce functions
    numOutputElements = vectorSize / (BLOCK_SIZE << 1);
    if ((vectorSize) % (BLOCK_SIZE << 1)) {
       numOutputElements++;
    }
    
    hostOutput = (float *)malloc(numOutputElements * sizeof(float));
    cudaMalloc(&deviceOutput, numOutputElements * sizeof(float));
	
    //Find and Print the mean using reduction
    float mean = findMean(deviceRatios, hostOutput, deviceOutput, numOutputElements, vectorSize);
	
    //print max and min and we don't need height (it's 1) so we just put it into a trash variable
    int temp = findHeight(deviceRatios, hostOutput, deviceOutput, numOutputElements, vectorSize, mean);
		
    /********************************Finding and fixing the signals for the matrix********************************************************/	
    float* hostSignalMatrix;
    float* deviceSignalMatrix;
    float* deviceSignalMatrixCopy;

    int sigMatrixNumElements = vectorSize;
    size_t sigMatrixNumBytes = sigMatrixNumElements * sizeof(float);
    
    hostSignalMatrix = (float *)malloc(sigMatrixNumBytes);
    cudaMalloc(&deviceSignalMatrix, sigMatrixNumBytes);
    cudaMalloc(&deviceSignalMatrixCopy, sigMatrixNumBytes);
    
    dim3 dimGrid((int)ceil(((float)vectorSize /BLOCK_SIZE_SMALL)));
    dim3 dimBlock(BLOCK_SIZE);

    cudaMemcpy( deviceSignalMatrix, hostSignalMatrix, sigMatrixNumBytes, cudaMemcpyHostToDevice);
    
    //Create the signal array using the best k value using same strategy as the above function
    signalMatrix1k<<<dimGrid, dimBlock>>>(deviceRatios, deviceSignalMatrix, mean, vectorSize, bestK);
    cudaDeviceSynchronize();
    
    cudaMemcpy(deviceSignalMatrixCopy, deviceSignalMatrix, sigMatrixNumBytes, cudaMemcpyDeviceToDevice);

    //Fix the signal array using the same strategy in the above function but for an array instead of a matrix
    signalFixGlobal<<<dimGrid, dimBlock>>>(deviceSignalMatrix,deviceSignalMatrixCopy,  vectorSize, 1);
    cudaDeviceSynchronize();
    	
    cudaMemcpy(hostSignalMatrix, deviceSignalMatrix, sigMatrixNumBytes, cudaMemcpyDeviceToHost);
   
    // Release device memory and host memory used for the function
    free(hostOutput);
    cudaFree(deviceOutput);
    
    //this function will find the profit using bestK and print it to the terminal
    profitOfBestK(deviceStockA, deviceStockB, deviceSignalMatrix, mean, vectorSize, bestK);
}


int main( int argc, char* argv[] )
{
    //Read AT&T Data
    std::vector<float> stockAData = readFloatData("data/ATT.csv");
    
    //Read Verizon Data
    std::vector<float> stockBData = readFloatData("data/VZ.csv");

    // Size of vectors
    int vectorSize = stockAData.size();
 
    // Host Data
    float *hostStockA;
    float *hostStockB;
    float *hostRatios;
 
    //Device Data
    float *deviceStockA;
    float *deviceStockB;
    float *deviceRatios;
 
    // Size, in bytes, of each vector
    size_t bytes = vectorSize * sizeof(float);
 
    // Allocate memory for each vector on host
    hostStockA = (float*)malloc(bytes);
    hostStockB = (float*)malloc(bytes);
    hostRatios = (float*)malloc(bytes);
 
    // Allocate memory for each vector on GPU
    cudaMalloc(&deviceStockA, bytes);
    cudaMalloc(&deviceStockB, bytes);
    cudaMalloc(&deviceRatios, bytes);

    // Initialize vectors on host
    for(int i = 0; i < vectorSize; i++ ) {
        hostStockA[i] = stockAData[i];
        hostStockB[i] = stockBData[i];
    }
 
    // Copy host vectors to device
    cudaMemcpy( deviceStockA, hostStockA, bytes, cudaMemcpyHostToDevice);
    cudaMemcpy( deviceStockB, hostStockB, bytes, cudaMemcpyHostToDevice);
 
    int blockSize, gridSize;
 
    // Number of threads in each thread block
    blockSize = BLOCK_SIZE;
 
    // Number of thread blocks in grid
    gridSize = (int)ceil((float)vectorSize / blockSize);
 
    // Execute the div kernel
    vecDiv<<<gridSize, blockSize>>>(deviceStockA, deviceStockB, deviceRatios, vectorSize);

    // Copy array back to host
    cudaMemcpy( hostRatios, deviceRatios, bytes, cudaMemcpyDeviceToHost );
    
    
    /**************************************** Generate Signal Matrices for the Training Session ********************************************/

    int trainVectorSize = vectorSize/2; //3894 for our data
    int testVectorSize = trainVectorSize/4; // 973 for our data
    int bestK1, bestK2, bestK3, bestK4;
	

    /*cout <<endl << "************************Training Session Test***************************" << endl;

    cudaMemcpy( deviceRatiosTraining, hostRatios, bytes/10, cudaMemcpyHostToDevice);
    bestK1 = Generate(deviceRatiosTraining,trainVectorSize);*/
    
    //We train on large array values. We test on smaller array values that comes after the training period//
    
    cout <<endl << "************************Training Session 1 (Day 1- Day 3894) (3894 days)************************" << endl;
    bestK1 = Generate(deviceStockA, deviceStockB,deviceRatios,trainVectorSize);
   
    /*test 
    cout <<endl << "********************Test for Testing Session 1 (Day 1- Day 3894)***************************" << endl;
    Generate(deviceStockA , deviceStockB ,deviceRatios,trainVectorSize, bestK1);*/
    
    cout <<endl << "************************Testing Session 1 (Day 3895 - Day 4868) (973 days)************************" << endl;
    Generate(deviceStockA + vectorSize/2, deviceStockB + vectorSize/2,deviceRatios+vectorSize/2,testVectorSize, bestK1);	
	
    cout <<endl << "************************Training Session 2 (Day 974 - Day 4868) (3894 days)************************" << endl;
    bestK2 = Generate(deviceStockA + vectorSize/8, deviceStockB + vectorSize/8, deviceRatios + vectorSize/8,trainVectorSize);
    
    /*test   
    cout <<endl << "************************Test for Training Session 2 (Day 4869 - Day 7788) (2921 days)********************************" << endl;
    int i = Generate(deviceStockA + 5*vectorSize/8, deviceStockB + 5*vectorSize/8, deviceRatios + 5*vectorSize/8,trainVectorSize - vectorSize/8);*/
    
    cout <<endl << "************************Testing Session 2 (Day 4869 - Day 7788) (2921 days)************************" << endl;
    //bestK2 = 11;    //This was for testing different k values
    Generate(deviceStockA + 5*vectorSize/8, deviceStockB + 5*vectorSize/8,deviceRatios+ 5*vectorSize/8,trainVectorSize - vectorSize/8,bestK2);
    
    cout <<endl << "************************Training Session 3 (Day 1947 - Day 5842) (3894 days)************************" << endl;
    bestK3 = Generate(deviceStockA+ vectorSize/4, deviceStockB+ vectorSize/4, deviceRatios + vectorSize/4,trainVectorSize);
    
    cout <<endl << "************************Testing Session 3 (Day 5843 - Day 6167) (324 days)************************" << endl;
    Generate(deviceStockA + 3*vectorSize/4, deviceStockB + 3*vectorSize/4,deviceRatios + 3*vectorSize/4,testVectorSize / 3, bestK3);   
    
    cout <<endl << "************************Training Session 4 (Day 5841 - Day 6814) (973 days)************************" << endl;
    bestK4 = Generate(deviceStockA+ 6*vectorSize/8, deviceStockB+ 6*vectorSize/8, deviceRatios +6*vectorSize/8,testVectorSize);
    
    cout <<endl << "************************Testing Session 4 (Day 6814 - Day 7788) (973 days)************************" << endl;
    Generate(deviceStockA + 7*vectorSize/8, deviceStockB + 7*vectorSize/8,deviceRatios + 7*vectorSize/8,testVectorSize, bestK4);

    //Use this if You want to see what the actual best K value was for the test interval
    //Generate(deviceStockA+ 3*vectorSize/8, deviceStockB+ 3*vectorSize/8, deviceRatiosTraining,trainVectorSize,bestK4);
    
    cout << cudaGetErrorString(cudaGetLastError()) << endl;

    // Release device memory
    cudaFree(deviceStockA);
    cudaFree(deviceStockB);
    cudaFree(deviceRatios);
 
    // Release host memory
    free(hostStockA);
    free(hostStockB);
    free(hostRatios);
 
    return 0;
}

