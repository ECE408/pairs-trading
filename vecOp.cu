/* This File will contain various vector operations which are all done using threads in parallel*/

__global__ void vecAdd(float *in1, float *in2, float *out, int len) {
	int Idx= threadIdx.x + blockDim.x * blockIdx.x;
	if (Idx<len) out[Idx] = in1[Idx] + in2[Idx];
}

__global__ void vecSub(float *in1, float *in2, float *out, int len) {
	int Idx= threadIdx.x + blockDim.x * blockIdx.x;
	if (Idx<len) out[Idx] = in1[Idx] - in2[Idx];
}

__global__ void vecMult(float *in1, float *in2, float *out, int len) {
	int Idx= threadIdx.x + blockDim.x * blockIdx.x;
	if (Idx<len) out[Idx] = in1[Idx] * in2[Idx];
}

__global__ void vecDiv(float *in1, float *in2, float *out, int len) {
	int Idx= threadIdx.x + blockDim.x * blockIdx.x;
	if (Idx<len) out[Idx] = in1[Idx] / in2[Idx];
}

// Given a list (lst) of length n
// Output its sum = lst[0] + lst[1] + ... + lst[n-1];
__global__ void total(float *input, float *output, int len) {
  
    //Load a segment of the input vector into shared memory
	__shared__ float partialSum[2 * BLOCK_SIZE];
	unsigned int tx = threadIdx.x;
	unsigned int start = 2 * blockIdx.x * blockDim.x;
	
	//Initialize Values
	partialSum[tx] = (start + tx < len) ? input[start + tx] : 0;
	partialSum[blockDim.x + tx] = (start + tx + BLOCK_SIZE < len) ? input[start + blockDim.x + tx] : 0;
	
	//Traverse the reduction tree
	unsigned int stride;
	for (stride = blockDim.x; stride > 0;  stride = stride / 2) {
		__syncthreads();
		if (tx < stride)
			partialSum[tx] += partialSum[tx + stride];
	}

	// Write the computed sum of the block to the output vector at the correct index
	if(tx ==0)
		output[blockIdx.x] = partialSum[0];
}

// Given a list (lst) of length n
// Output its max = max(lst[0],lst[1],...,lst[n-1]);
__global__ void max(float *input, float *output, int len) {

    //Load a segment of the input vector into shared memory 	
	__shared__ float partialMax[2 * BLOCK_SIZE];
	unsigned int tx = threadIdx.x;
	unsigned int start = 2 * blockIdx.x * blockDim.x;

	//Initialize Values	
	partialMax[tx] = (start + tx < len) ? input[start + tx] : 0;
	partialMax[blockDim.x + tx] = (start + tx + BLOCK_SIZE < len) ? input[start + blockDim.x + tx] : 0;
	
	//Traverse the reduction tree
	unsigned int stride;
	for (stride = blockDim.x; stride > 0;  stride = stride / 2) {
		__syncthreads();
		if (tx < stride)
			partialMax[tx] = max(partialMax[tx + stride],partialMax[tx]);
	}
	
	// Write the computed sum of the block to the output vector at the correct index
	if(tx ==0)
		output[blockIdx.x] = partialMax[0];
}

// Given a list (lst) of length n
// Output its min = min(lst[0],lst[1],...,lst[n-1]);
__global__ void min(float *input, float *output, int len) {

    //Load a segment of the input vector into shared memory 	
	__shared__ float partialMin[2 * BLOCK_SIZE];
	unsigned int tx = threadIdx.x;
	unsigned int start = 2 * blockIdx.x * blockDim.x;

	//Initialize Values	
	partialMin[tx] = (start + tx < len) ? input[start + tx] : INFINITY;
	partialMin[blockDim.x + tx] = (start + tx + BLOCK_SIZE < len) ? input[start + blockDim.x + tx] : INFINITY;
	
	//Traverse the reduction tree
	unsigned int stride;
	for (stride = blockDim.x; stride > 0;  stride = stride / 2) {
		__syncthreads();
		if (tx < stride)
			partialMin[tx] = min(partialMin[tx + stride],partialMin[tx]);
	}
	
	// Write the computed sum of the block to the output vector at the correct index
	if(tx ==0)
		output[blockIdx.x] = partialMin[0];
}
