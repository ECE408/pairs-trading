
__global__ void signalMatrix1k(float *ratios, float *signalMatrix, float mean, int ratioSize, int bestk) {

    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    float result = 0.0;

    __shared__ float subRatios[BLOCK_SIZE + 1];

    if ( threadIdx.y == 0 && idx < ratioSize ) {
        if ( threadIdx.x == 0 && idx != 0 ) {
            subRatios[threadIdx.x] = ratios[idx - 1];
        }
        subRatios[threadIdx.x + 1] = ratios[idx];
    }

    __syncthreads();

    if ( idx < ratioSize) {
        float prevRatio = subRatios[threadIdx.x];
        float curRatio =  subRatios[threadIdx.x + 1];

		float k = (float)(bestk+1)/100.0;

        float mpk = (mean + k);
        float mmk = (mean - k);

        if ( (prevRatio > mean && curRatio <= mean)  ||  (prevRatio < mean && curRatio >= mean) || (prevRatio == mean && curRatio == mean) ) {
	    result = 999.0;
        } else if (      ( (prevRatio < mpk) && (curRatio >= mpk) )       ||       ( (prevRatio > mpk) && (curRatio <= mpk) )    ||   ( (prevRatio == mpk) && (curRatio == mpk) )  ) {
            result = -1.0;
        } else if (      ( (prevRatio < mmk) && (curRatio >= mmk) )       ||       ( (prevRatio > mmk) && (curRatio <= mmk) )     ||   ( (prevRatio == mmk) && (curRatio == mmk) )  ) {
            result = 1.0;
        }

	if (idx == 0) result = 0;

        signalMatrix[idx] = result;
    }
}
