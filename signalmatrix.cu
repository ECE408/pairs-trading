/* Populate the signal matrix with values depending on when the ratio cross the different k values. If it crosses mean + k, fill the element with -1,
if it crosses mean - k, fill the element with 1, if it crosses the mean, fill the elemement with 9 which will be fixed using a different kernel 
function.
*/
__global__ void signalMatrix(float *ratios, float *signalMatrix, float mean, int ratioSize, int sigMatrixHeight) {

    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    int idy = threadIdx.y + blockDim.y * blockIdx.y;
    float result = 0.0;

    __shared__ float subRatios[BLOCK_SIZE_SMALL + 1];

    if ( threadIdx.y == 0 && idx < ratioSize ) {
        if ( threadIdx.x == 0 && idx != 0 ) {
            subRatios[threadIdx.x] = ratios[idx - 1];
        }
        subRatios[threadIdx.x + 1] = ratios[idx];
    }

    __syncthreads();

    if ( idx < ratioSize && idy < sigMatrixHeight ) {
        float prevRatio = subRatios[threadIdx.x];
        float curRatio =  subRatios[threadIdx.x + 1];

        float k = (float)(idy + 1) / 100.0;

        float mpk = (mean + k);
        float mmk = (mean - k);

        if ( (prevRatio > mean && curRatio <= mean)  ||  (prevRatio < mean && curRatio >= mean) || (prevRatio == mean && curRatio == mean) ) {
	    result = 999.0;
        } else if (      ( (prevRatio < mpk) && (curRatio >= mpk) )       ||       ( (prevRatio > mpk) && (curRatio <= mpk) )    ||   ( (prevRatio == mpk) && (curRatio == mpk) )  ) {
            result = -1.0;
        } else if (      ( (prevRatio < mmk) && (curRatio >= mmk) )       ||       ( (prevRatio > mmk) && (curRatio <= mmk) )     ||   ( (prevRatio == mmk) && (curRatio == mmk) )  ) {
            result = 1.0;
        }

	if (idx == 0) result = 0;

        signalMatrix[(idy * ratioSize) + idx] = result;
    }
}
