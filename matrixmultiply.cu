#include "wb.h"
using namespace std;

#define TILE_WIDTH      32


// Compute C = A * B
__global__ void matrixMultiplyShared(float *A, float *B, float *C, int numARows,
                                     int numAColumns, int numBRows,
                                     int numBColumns, int numCRows,
                                     int numCColumns) {
  //Shared tile implementation
	__shared__ float subTileA[TILE_WIDTH][TILE_WIDTH];
	__shared__ float subTileB[TILE_WIDTH][TILE_WIDTH];
	
	int bx = blockIdx.x;	int by = blockIdx.y;
	int tx = threadIdx.x;	int ty = threadIdx.y;
		
	//invalid matrix multiplication
	if (numAColumns != numBRows)
		return;
	
	int Width = numAColumns;
	
	//Identify the row and column of the C element to work on
	int Row = by * TILE_WIDTH + ty;
	int Col = bx * TILE_WIDTH + tx;
	float Cvalue = 0.0;
	
	//Loop over the A and B tiles required to compute the P element
		for (int m = 0; m < ceil(numAColumns/(float)TILE_WIDTH); m++){
			//Collaborate Loading of A and B tiles to shared memory
			if (m*TILE_WIDTH + tx < numAColumns && Row < numARows)
				subTileA[ty][tx] = A[Row*Width + m*TILE_WIDTH+tx];   
			else
				subTileA[ty][tx] = 0.0;
			if (m*TILE_WIDTH + ty < numBRows && Col < numBColumns)
				subTileB[ty][tx] = B[(m*TILE_WIDTH+ty)*numBColumns+Col];
			else
				subTileB[ty][tx] = 0.0;
			__syncthreads();
			if(Row < numARows && Col < numBColumns)
				for (int k = 0; k < TILE_WIDTH; k++)
					Cvalue += subTileA[ty][k] * subTileB[k][tx];
			__syncthreads();
		}
		if((Row < numCRows) && (Col < numCColumns))
			C[Row*numCColumns+Col] = Cvalue;		
	
}


