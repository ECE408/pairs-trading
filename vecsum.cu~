// MP Reduction
// Given a list (lst) of length n
// Output its sum = lst[0] + lst[1] + ... + lst[n-1];

__global__ void total(float *input, float *output, int len) {
  //@@ Load a segment of the input vector into shared memory
  //@@ Traverse the reduction tree
  //@@ Write the computed sum of the block to the output vector at the
  //@@ correct index
	__shared__ float partialSum[2 * BLOCK_SIZE];
	unsigned int tx = threadIdx.x;
	unsigned int start = 2 * blockIdx.x * blockDim.x;
	
	partialSum[tx] = (start + tx < len) ? input[start + tx] : 0;
	partialSum[blockDim.x + tx] = (start + tx + BLOCK_SIZE < len) ? input[start + blockDim.x + tx] : 0;
	
	unsigned int stride;
	for (stride = blockDim.x; stride > 0;  stride = stride / 2) {
		__syncthreads();
		if (tx < stride)
			partialSum[tx] += partialSum[tx + stride];
	}
	
	if(tx ==0)
		output[blockIdx.x] = partialSum[0];
}
